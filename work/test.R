options(width = 110)

library("pdfmole")
library("pdfminer")
library("pdfboxr")

pdffile <- system.file("pdfs/cars.pdf", package = "pdfboxr")
pdfm <- pdfminer::read_chars(pdffile)
pdfb <- pdfboxr::read_chars(pdffile, adjust = TRUE, max_memory = 4096L)

args(pdfboxr::read_chars)

head(pdfm$text, 20)
head(pdfb$text, 20)

tail(pdfm$text, 20)
tail(pdfb$text, 20)


df <- pdfb$text
head(df)

pixelplot(df, scale = 0.5, cex.axis = 0.55)

bboxplot(df, pid = 1L)

textplot(df, pid = 1L)

df$y0 <- df$y1 - df$height
head(df, 20)

textplot(df, pid = 1L)
pixelplot(df, scale = 0.5, cex.axis = 0.55)

df <- align_rows(df)
df <- align_columns(df, split_points = c(78, 126, 220))
df <- df[grep("Courier", df$font),]
df <- group_columns(df)
x <- mole(df, header = TRUE, simplify = TRUE)
head(as.data.frame(x))

head(cars)


# pdfminer 
#    pid block text         font size colorspace     color    x0      y0    x1      y1
# 1    1     1    s Courier-Bold   12 DeviceGray [0, 0, 0]  77.2 751.272  84.4 763.272
# 2    1     1    p Courier-Bold   12 DeviceGray [0, 0, 0]  84.4 751.272  91.6 763.272
# 3    1     1    e Courier-Bold   12 DeviceGray [0, 0, 0]  91.6 751.272  98.8 763.272
# 4    1     1    e Courier-Bold   12 DeviceGray [0, 0, 0]  98.8 751.272 106.0 763.272
# 5    1     1    d Courier-Bold   12 DeviceGray [0, 0, 0] 106.0 751.272 113.2 763.272
#
# 7    1     2    d Courier-Bold   12 DeviceGray [0, 0, 0] 124.4 751.272 131.6 763.272
# 8    1     2    i Courier-Bold   12 DeviceGray [0, 0, 0] 131.6 751.272 138.8 763.272
# 9    1     2    s Courier-Bold   12 DeviceGray [0, 0, 0] 138.8 751.272 146.0 763.272
# 10   1     2    t Courier-Bold   12 DeviceGray [0, 0, 0] 146.0 751.272 153.2 763.272

# pdfbox
#    pid text                  font size     x0    y0     x1    y1 width height yscale xscale rotation
# 5    1    s          Courier-Bold   12  77.20  88.4  84.40 753.6  7.20   6.31     12     12        0
# 6    1    p          Courier-Bold   12  84.40  88.4  91.60 753.6  7.20   6.31     12     12        0
# 7    1    e          Courier-Bold   12  91.60  88.4  98.80 753.6  7.20   6.31     12     12        0
# 8    1    e          Courier-Bold   12  98.80  88.4 106.00 753.6  7.20   6.31     12     12        0
# 9    1    d          Courier-Bold   12 106.00  88.4 113.20 753.6  7.20   6.31     12     12        0
#
# 10   1    d          Courier-Bold   12 124.40  88.4 131.60 753.6  7.20   6.31     12     12        0
# 11   1    i          Courier-Bold   12 131.60  88.4 138.80 753.6  7.20   6.31     12     12        0
# 12   1    s          Courier-Bold   12 138.80  88.4 146.00 753.6  7.20   6.31     12     12        0
# 13   1    t          Courier-Bold   12 146.00  88.4 153.20 753.6  7.20   6.31     12     12        0

# pdfbox - adjust = FALSE
# 5    1    s          Courier-Bold   12  77.20  88.4  84.40 753.6  7.20   6.31     12     12        0
# 6    1    p          Courier-Bold   12  84.40  88.4  91.60 753.6  7.20   6.31     12     12        0
# 7    1    e          Courier-Bold   12  91.60  88.4  98.80 753.6  7.20   6.31     12     12        0
# 8    1    e          Courier-Bold   12  98.80  88.4 106.00 753.6  7.20   6.31     12     12        0
# 9    1    d          Courier-Bold   12 106.00  88.4 113.20 753.6  7.20   6.31     12     12        0
#
# 10   1    d          Courier-Bold   12 124.40  88.4 131.60 753.6  7.20   6.31     12     12        0
# 11   1    i          Courier-Bold   12 131.60  88.4 138.80 753.6  7.20   6.31     12     12        0
# 12   1    s          Courier-Bold   12 138.80  88.4 146.00 753.6  7.20   6.31     12     12        0
# 13   1    t          Courier-Bold   12 146.00  88.4 153.20 753.6  7.20   6.31     12     12        0

